
## acl

acl "trusted" {
        192.168.1.0/24;    # LAN1 network
        192.168.2.0/24;    # LAN2 network
        192.168.3.0/24;    # LAN3 network
};

## options

options {
    directory "/var/bind";              
    version "private";
    pid-file "/var/run/named/named.pid";
    dnssec-enable no;                     # Disable DNSSEC support
    dnssec-validation no;                 # Disable DNSSEC validation
    recursion yes;                        # If a DNS query requests recursion, then the server will attempt to do all the work required to answer the query
    allow-recursion { trusted; };         # Specifies which hosts are allowed to make recursive queries through this server
    allow-query { any; };                 # Specifies which hosts are allowed to ask ordinary DNS questions
    listen-on port 53 { any; };           # Enable IPv4
    listen-on-v6 { none; };               # Disable IPv6
    allow-transfer { 192.168.1.3; };      # Specifies which hosts are allowed to receive zone transfers from the server
    empty-zones-enable no;                # Enable or disable all empty zones. By default, they are enabled
    forwarders { 192.168.3.2; };          # Used to override the list of global forwarders
};

## rndc

key "rndc-key" {
    algorithm hmac-sha256;
    secret "vdjYbo22cUUsJWTIq7RsPIXjSxKyCLcC5greD3WUJh8=";
};
 
controls {
    inet 127.0.0.1 port 953
    allow { 127.0.0.1; } keys { "rndc-key"; };
};

## zones

zone "edu" IN {
    type slave;                           # Act as slave
    file "/var/bind/db.edu";              # Forward zone file path
    masters { 192.168.1.2; };             # Ip of primary ns
};

zone "1.168.192.in-addr.arpa" IN {
    type slave;                           # Act as slave
    file "/var/bind/db.1.168.192";        # Reverse zone file path for 192.168.1.0/24 subnet
    masters { 192.168.1.2; };             # Ip of primary ns
};