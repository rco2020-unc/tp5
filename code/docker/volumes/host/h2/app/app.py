from flask import Flask
from flask import jsonify
from flask import request
from flask import render_template
import subprocess, webbrowser

app = Flask(__name__)

alumnos = [{'id': '1', 'nombre': 'Ezequiel', 'apellido': 'Zimmel'},
           {'id': '2', 'nombre': 'Christian','apellido': 'Nicolaide'},
           {'id': '3', 'nombre': 'Alumno_A', 'apellido': 'Apellido_A'},
           {'id': '4', 'nombre': 'Alumno_B', 'apellido': 'Apellido_B'}]

@app.route('/', methods=['GET'])
def index():
    #print(request.headers)
    return render_template('index.html')

@app.route('/alumnos', methods=['GET'])
def returnAll():
    return jsonify({'alumnos': alumnos})

@app.route('/alumnos/<string:id>', methods=['GET'])
def returnOne(id):
    theOne = alumnos[0]
    for i, q in enumerate(alumnos):
        if q['id'] == id:
            theOne = alumnos[i]
    return jsonify({'alumnos': theOne})

@app.route('/alumnos', methods=['POST'])
def addOne():
    new_student = request.get_json()
    alumnos.append(new_student)
    return jsonify({'alumnos': alumnos})

@app.route('/alumnos/<string:id>', methods=['PUT'])
def editOne(id):
    new_student = request.get_json()
    for i, q in enumerate(alumnos):
        if q['id'] == id:
            alumnos[i] = new_student
    qs = request.get_json()
    return jsonify({'alumnos': alumnos})

@app.route('/alumnos/<string:id>', methods=['DELETE'])
def deleteOne(id):
    for i, q in enumerate(alumnos):
        if q['id'] == id:
            del alumnos[i]
            return jsonify({'alumnos': alumnos})
            break
    return jsonify({'ERROR 404':'User id not found!'})

if __name__ == "__main__":
    app.debug = True
    app.run()