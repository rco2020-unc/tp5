build:
	@echo "\n\e[1m\e[32m-- Building docker images --\e[22m\e[24m\e[39m"
	docker-compose build
	
setup:
	@echo "\n\e[1m\e[32m-- Mounting containers --\e[22m\e[24m\e[39m"
	docker-compose up -d
	docker exec -ti host_H1 /usr/sbin/dnsmasq -q
	docker cp ./volumes/host/h2/app/templates/ host_H2:/app/templates

clean:
	@echo "\n\e[1m\e[32m-- Removing containers --\e[22m\e[24m\e[39m"
	docker-compose down --remove-orphans

remove:
	make clean
	@echo "\n\e[1m\e[32m-- Pruning docker images and networks --\e[22m\e[24m\e[39m"
	docker system prune -a

configure:
########### IPv4 config ########### 	
	@echo "\n\e[1m\e[32m-- Configuring default IPv4 gateway on nodes --\e[22m\e[24m\e[39m"
# pc1.edu
	docker exec -ti host_H1 ip route del default;
	docker exec -ti host_H1 ip route add default via 192.168.1.10;
# pc2.edu
	docker exec -ti host_H2 ip route del default;
	docker exec -ti host_H2 ip route add default via 192.168.1.10;
# ns1.edu
	docker exec -ti dns1_primario ip route del default;
	docker exec -ti dns1_primario ip route add default via 192.168.1.10;
# ns2.edu
	docker exec -ti dns1_secundario ip route del default;
	docker exec -ti dns1_secundario ip route add default via 192.168.1.10;
# ns1.com
	docker exec -ti dns2_primario ip route del default;
	docker exec -ti dns2_primario ip route add default via 192.168.2.10;
# pc3.com
	docker exec -ti host_H3 ip route del default;
	docker exec -ti host_H3 ip route add default via 192.168.2.10;
# root
	docker exec -ti dns3_root ip route del default;
	docker exec -ti dns3_root ip route add default via 192.168.3.10;

########### IPv6 config ########### 
	@echo "\n\e[1m\e[32m-- Configuring default IPv6 gateway on nodes --\e[22m\e[24m\e[39m"
# pc1.edu
	docker exec -ti host_H1 ip -6 route del default;
	docker exec -ti host_H1 ip -6 route add default via 2001:a:a:1::10;
# pc2.edu
	docker exec -ti host_H2 ip -6 route del default;
	docker exec -ti host_H2 ip -6 route add default via 2001:a:a:1::10;
# ns1.edu
	docker exec -ti dns1_primario ip -6 route del default;
	docker exec -ti dns1_primario ip -6 route add default via 2001:a:a:1::10;
# ns2.edu
	docker exec -ti dns1_secundario ip -6 route del default;
	docker exec -ti dns1_secundario ip -6 route add default via 2001:a:a:1::10;
# ns1.com
	docker exec -ti dns2_primario ip -6 route del default;
	docker exec -ti dns2_primario ip -6 route add default via 2001:b:b:2::10;
# pc3.com
	docker exec -ti host_H3 ip -6 route del default;
	docker exec -ti host_H3 ip -6 route add default via 2001:b:b:2::10;
# root
	docker exec -ti dns3_root ip -6 route del default;
	docker exec -ti dns3_root ip -6 route add default via 2001:c:c:3::10;
		
check:
	@echo "\n\e[1m\e[32m-- Checking IPv4 and IPv6 connectivity --\e[22m\e[24m\e[39m"
# tests desde pc1.edu a pc2.edu
	@echo "\n\e[1m\e[32mping de pc1 a pc2\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 ping -c 3 192.168.1.5;
	docker exec -ti host_H1 ping6 -c 3 2001:a:a:1::5;
	@echo "\n\e[1m\e[32mnslookup de pc1 a pc2\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 nslookup pc2.edu
	@echo "\n\e[1m\e[32mnslookup inverso de pc1 a pc2\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 nslookup 192.168.1.5

# tests desde pc1.edu a pc3.com	
	@echo "\n\e[1m\e[32mping de pc1 a pc3\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 ping -c 3 192.168.2.2;
	docker exec -ti host_H1 ping6 -c 3 2001:b:b:2::2;
	@echo "\n\e[1m\e[32mprobando cache dnsmasq con drill\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 drill pc3.com | grep "Query time"
	docker exec -ti host_H1 drill pc3.com | grep "Query time"
	@echo "\n\e[1m\e[32mnslookup de pc1 a pc3 usando primario (ns1.edu)\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 nslookup pc3.com 192.168.1.2
	@echo "\n\e[1m\e[32mnslookup inverso de pc1 a pc3 usando primario (ns1.edu)\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 nslookup 192.168.2.2 192.168.1.2
	@echo "\n\e[1m\e[32mnslookup de pc1 a pc3 usando secundario (ns2.edu)\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 nslookup pc3.com 192.168.1.3
	@echo "\n\e[1m\e[32mnslookup inverso de pc1 a pc3 usando secundario (ns2.edu)\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 nslookup 192.168.2.2 192.168.1.3
	# Por algun motivo esto no me anda ami, lo comento por ahora
	#@echo "\n\e[1m\e[32mVuelco de memoria y listado de cache dnsmasq\e[22m\e[24m\e[39m"
	#docker exec -ti host_H1 pkill -USR1 dnsmasq
	#docker exec -ti host_H1 cat /var/log/dnsmasq.log

# tests desde pc2.edu a pc1.edu	
	@echo "\n\e[1m\e[32mping de pc2 a pc1\e[22m\e[24m\e[39m"
	docker exec -ti host_H2 ping -c 3 192.168.1.4;
	docker exec -ti host_H2 ping6 -c 3 2001:a:a:1::4;
	@echo "\n\e[1m\e[32mnslookup de pc2 a pc1\e[22m\e[24m\e[39m"
	docker exec -ti host_H2 nslookup pc1.edu
	@echo "\n\e[1m\e[32mnslookup inverso de pc2 a pc1\e[22m\e[24m\e[39m"
	docker exec -ti host_H2 nslookup 192.168.1.4

# tests desde pc2.edu a pc3.com	
	@echo "\n\e[1m\e[32mping de pc2 a pc3\e[22m\e[24m\e[39m"
	docker exec -ti host_H2 ping -c 3 192.168.2.2;
	docker exec -ti host_H2 ping6 -c 3 2001:b:b:2::2;
	@echo "\n\e[1m\e[32mnslookup de pc2 a pc3\e[22m\e[24m\e[39m"
	docker exec -ti host_H2 nslookup pc3.com
	@echo "\n\e[1m\e[32mnslookup inverso de pc2 a pc3\e[22m\e[24m\e[39m"
	docker exec -ti host_H2 nslookup 192.168.2.2	

# tests desde pc3.com a pc1.edu
	@echo "\n\e[1m\e[32mping de pc3 a pc1\e[22m\e[24m\e[39m"
	docker exec -ti host_H3 ping -c 3 192.168.1.4;
	docker exec -ti host_H3 ping6 -c 3 2001:a:a:1::4;
	@echo "\n\e[1m\e[32mnslookup de pc3 a pc1\e[22m\e[24m\e[39m"
	docker exec -ti host_H3 nslookup pc1.edu
	@echo "\n\e[1m\e[32mnslookup inverso de pc3 a pc1\e[22m\e[24m\e[39m"
	docker exec -ti host_H3 nslookup 192.168.1.4

# tests desde pc3.com a pc2.edu
	@echo "\n\e[1m\e[32mPing de pc3 a pc2\e[22m\e[24m\e[39m"
	docker exec -ti host_H3 ping -c 3 192.168.1.5;
	docker exec -ti host_H3 ping6 -c 3 2001:a:a:1::5;
	@echo "\n\e[1m\e[32mnslookup de pc3 a pc2\e[22m\e[24m\e[39m"
	docker exec -ti host_H3 nslookup pc2.edu
	@echo "\n\e[1m\e[32mnslookup inverso de pc3 a pc2\e[22m\e[24m\e[39m"
	docker exec -ti host_H3 nslookup 192.168.1.5

# checkeo sincronismo entre ns1.edu y ns2.edu
	@echo "\n\e[1m\e[32mcheckeo que dns secundario reciba las zonas \e[22m\e[24m\e[39m"
	docker exec -ti dns1_secundario rndc dumpdb -zones;
	docker exec -ti dns1_secundario cat /var/bind/named_dump.db;

# tests webserver (pc2.edu)
	@echo "\n\e[1m\e[32mtest Flask API de pc1 a pc2\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 curl http://192.168.1.5/alumnos
	docker exec -ti host_H1 curl http://192.168.1.5/alumnos -d '{"id":"5", "nombre":"Check", "nombre":"POST"}' -H 'Content-Type: application/json'
	docker exec -ti host_H1 curl -X "DELETE" http://192.168.1.5/alumnos/5
	@echo "\n\e[1m\e[32mtest Flask API de pc3 a pc2\e[22m\e[24m\e[39m"
	docker exec -ti host_H3 curl http://pc2.edu/alumnos
	docker exec -ti host_H3 curl http://pc2.edu/alumnos -d '{"id":"5", "nombre":"Check_2", "nombre":"POST_2"}' -H 'Content-Type: application/json'
	docker exec -ti host_H3 curl -X "DELETE" http://pc2.edu/alumnos/5	

reconfigure:
	make clean
	make build
	make setup
	make configure
	make check